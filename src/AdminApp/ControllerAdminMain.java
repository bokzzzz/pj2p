package AdminApp;

import AdminService.Vehicle.Car;
import AdminService.Vehicle.Van;
import AdminService.Vehicle.Vehicle;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class ControllerAdminMain implements Initializable {
    public static final List<Vehicle> listVehicle = new ArrayList<>();
    static Vehicle veh;
    @FXML
    private ComboBox vehicleCB;
    @FXML
    private ComboBox typeCB;
    @FXML
    private Button addBT;
    @FXML
    private Button removeBT;
    @FXML
    private Button changeBT;
    @FXML
    private TableView tableView;

    @FXML
    public static void alertFunc(String title, String contentText, Alert.AlertType type, ButtonType btType) {
        Alert alert = new Alert(type, null, btType);
        alert.setTitle(title);
        alert.setContentText(contentText);
        alert.show();
    }

    @FXML
    private void add() {
        try {
            String vehicle = vehicleCB.getSelectionModel().getSelectedItem().toString();
            String type = typeCB.getSelectionModel().getSelectedItem().toString();
            ControllerAddVehicle.isSiren = ControllerAddVehicle.isCapacity = ControllerAddVehicle.isDoors = false;
            switch (vehicle) {
                case "Car":
                    ControllerAddVehicle.isDoors = true;
                    if (!type.equals("Civil")) {
                        ControllerAddVehicle.isSiren = true;
                    }
                    break;
                case "Van":
                    ControllerAddVehicle.isCapacity = true;
                    if (!type.equals("Civil")) {
                        ControllerAddVehicle.isSiren = true;
                    }
                    break;
                default:
                    if (!type.equals("Civil")) {
                        ControllerAddVehicle.isSiren = true;
                    }
                    break;
            }
            switch (type) {
                case "Police":
                    ControllerAddVehicle.type = 2;
                    break;
                case "Ambulance":
                    ControllerAddVehicle.type = 0;
                    break;
                case "Civil":
                    ControllerAddVehicle.type = 3;
                    break;
                default:
                    ControllerAddVehicle.type = 1;
                    break;
            }
            FXMLLoad();
            update();
            reset();
        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @FXML
    private void setColumn() {
        TableColumn<Vehicle, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Vehicle, String> chassisNumberColumn = new TableColumn<>("Chassis number");
        chassisNumberColumn.setCellValueFactory(new PropertyValueFactory<>("chassisNumber"));

        TableColumn<Vehicle, String> engineNumberColumn = new TableColumn<>("Engine number");
        engineNumberColumn.setCellValueFactory(new PropertyValueFactory<>("engineNumber"));


        TableColumn<Vehicle, String> licencePlateColumn = new TableColumn<>("Licence plate");
        licencePlateColumn.setCellValueFactory(new PropertyValueFactory<>("licencePlate"));

        TableColumn<Vehicle, Integer> currentPlNumberColumn = new TableColumn<>("Platform number");
        currentPlNumberColumn.setCellValueFactory(new PropertyValueFactory<>("currentPlNumber"));

        TableColumn<Vehicle, Integer> XPosition = new TableColumn<>("Position x");
        XPosition.setCellValueFactory(new PropertyValueFactory<>("XPosition"));

        TableColumn<Vehicle, Integer> YPosition = new TableColumn<>("Position y");
        YPosition.setCellValueFactory(new PropertyValueFactory<>("YPosition"));

        tableView.getColumns().addAll(nameColumn, chassisNumberColumn, engineNumberColumn,
                licencePlateColumn, currentPlNumberColumn, XPosition, YPosition);

        nameColumn.prefWidthProperty().bind(tableView.widthProperty().divide(7));
        chassisNumberColumn.prefWidthProperty().bind(tableView.widthProperty().divide(7));
        engineNumberColumn.prefWidthProperty().bind(tableView.widthProperty().divide(7));
        licencePlateColumn.prefWidthProperty().bind(tableView.widthProperty().divide(7));
        currentPlNumberColumn.prefWidthProperty().bind(tableView.widthProperty().divide(7));
        XPosition.prefWidthProperty().bind(tableView.widthProperty().divide(7));
        YPosition.prefWidthProperty().bind(tableView.widthProperty().divide(7));
    }

    @FXML
    private void remove() {

        if (!tableView.getItems().isEmpty()) {
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                int i = tableView.getSelectionModel().getSelectedIndex();
                listVehicle.remove(tableView.getSelectionModel().getSelectedItem());
                tableView.getItems().remove(i);
            } else {
                alertFunc("Warning", "Vehicle is not selected!", Alert.AlertType.WARNING, ButtonType.OK);
            }
        } else {
            alertFunc("Warning", "Table is empty!", Alert.AlertType.WARNING, ButtonType.OK);
        }
    }

    @FXML
    private void change() {
        ControllerAddVehicle.isCapacity = ControllerAddVehicle.isSiren = ControllerAddVehicle.isDoors = false;
        if (!tableView.getItems().isEmpty()) {
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                veh = (Vehicle) tableView.getSelectionModel().getSelectedItem();
                ControllerAddVehicle.isChange = true;
                ControllerAddVehicle.chassisNameCG = veh.getChassisNumber();
                ControllerAddVehicle.nameCG = veh.getName();
                ControllerAddVehicle.platformNumberCG = veh.getCurrentPlNumber();
                ControllerAddVehicle.xPositionCG = veh.getXPosition();
                ControllerAddVehicle.yPositionCG = veh.getYPosition();
                ControllerAddVehicle.licencePlateCG = veh.getLicencePlate();
                ControllerAddVehicle.engineNameCG = veh.getEngineNumber();
                ControllerAddVehicle.type = veh.getType();
                ControllerAddVehicle.img = veh.getImage();
                if (veh.getType() < 3) {
                    ControllerAddVehicle.isSiren = true;
                    ControllerAddVehicle.sirenCG = veh.isRotationOn();
                    if (veh.getType() == 2) ControllerAddVehicle.policeF = veh.getPoliceFile().toString();
                }
                if (veh instanceof Car) {
                    ControllerAddVehicle.isDoors = true;
                    ControllerAddVehicle.doorsNumberCG = ((Car) veh).getNumberOfDors();
                } else if (veh instanceof Van) {
                    ControllerAddVehicle.isCapacity = true;
                    ControllerAddVehicle.capacityCG = ((Van) veh).getCapacity();
                }
                int i = tableView.getSelectionModel().getSelectedIndex();
                tableView.getItems().remove(i);
                listVehicle.remove(veh);
                FXMLLoad();
                update();
            } else {
                alertFunc("Warning!!!!!", "Vehicle is not selected!", Alert.AlertType.WARNING, ButtonType.OK);
            }
        } else {
            alertFunc("Warning", "Table is empty!", Alert.AlertType.WARNING, ButtonType.OK);
        }
    }

    @FXML
    private void ActionOnType() {
        if (vehicleCB.getSelectionModel().getSelectedItem() != null)
            addBT.setDisable(false);
    }

    @FXML
    private void firstActionOnCombo() {
        if (vehicleCB.getSelectionModel().getSelectedItem().equals("Car")) {
            ObservableList<String> type = FXCollections.observableArrayList("Police", "Civil", "Ambulance");
            typeCB.setItems(type);
        }
        if (vehicleCB.getSelectionModel().getSelectedItem().equals("Van")) {
            ObservableList<String> type = FXCollections.observableArrayList("Police", "Civil", "Ambulance", "Firefighter");
            typeCB.setItems(type);
        }
        if (vehicleCB.getSelectionModel().getSelectedItem().equals("Motorcycle")) {
            ObservableList<String> type = FXCollections.observableArrayList("Police", "Civil");
            typeCB.setItems(type);
        }
        typeCB.setDisable(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> vehicle = FXCollections.observableArrayList("Car", "Motorcycle", "Van");
        setColumn();
        ObservableList<Vehicle> tableFill = FXCollections.observableArrayList();
        tableView.setItems(tableFill);
        vehicleCB.setItems(vehicle);
        vehicleCB.valueProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            if (newValue != null) {
                firstActionOnCombo();
            }
        });

        reset();
        reload();
    }

    private void update() {
        tableView.getItems().add(veh);
        listVehicle.add(veh);
    }

    private void FXMLLoad() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("addVehicle.fxml"));
            Stage stage1 = new Stage();
            stage1.initOwner(AdminMain.stage);
            stage1.setScene(new Scene(loader.load()));
            stage1.initModality(Modality.APPLICATION_MODAL);
            stage1.setTitle("Add Vehicle");
            ControllerAddVehicle.stage = stage1;
            stage1.showAndWait();
        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }

    private void reset() {
        typeCB.setDisable(true);
        typeCB.getSelectionModel().clearSelection();
        vehicleCB.getSelectionModel().clearSelection();
        addBT.setDisable(true);

    }

    @FXML
    private void save() {
        try {
            FileOutputStream fos = new FileOutputStream("garage.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(listVehicle);
            oos.close();
            fos.close();
        } catch (IOException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }

    private void reload() {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File("garage.ser")))) {
            List<Vehicle> list = (ArrayList) in.readObject();
            listVehicle.addAll(list);
            tableView.getItems().addAll(list);
        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @FXML
    private void startUserApp() {
        try {
            save();
            Parent root = FXMLLoader.load(getClass().getResource("/UserApp/UserAPP.fxml"));
            Stage stage = (Stage) addBT.getScene().getWindow();
            stage.setTitle("Garage");
            stage.setScene(new Scene(root, 421, 550));
            stage.centerOnScreen();
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @FXML
    private void exit() {
        save();
        System.exit(0);
    }
}
