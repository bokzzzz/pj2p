package AdminApp;

import AdminService.BadInputException;
import AdminService.Vehicle.Car;
import AdminService.Vehicle.Motorcycle;
import AdminService.Vehicle.Van;
import AdminService.Vehicle.Vehicle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class ControllerAddVehicle implements Initializable {
    static boolean isSiren = false, isCapacity = false, isDoors = false, sirenCG;
    static int type, platformNumberCG, xPositionCG, yPositionCG, doorsNumberCG, capacityCG;
    static String nameCG, chassisNameCG, engineNameCG, licencePlateCG, policeF;
    static boolean isChange = false;
    static File img;
    static Stage stage;
    @FXML
    private TextField capacityTF;
    @FXML
    private Label capacityLL;
    @FXML
    private Label dorsLL;
    @FXML
    private Label policeFL;
    @FXML
    private ChoiceBox chBoxDoors;
    @FXML
    private Label sirenLL;
    @FXML
    private ChoiceBox chBoxSiren;
    @FXML
    private Button openBT;
    @FXML
    private Button openPoliceBT;
    @FXML
    private ImageView imageV;
    @FXML
    private Button addBT;
    @FXML
    private TextField nameTF;
    @FXML
    private TextField chassisNameTF;
    @FXML
    private TextField policeFTF;
    @FXML
    private TextField engineNumberTF;
    @FXML
    private TextField licencePlateTF;
    @FXML
    private TextField platformNumberTF;
    @FXML
    private TextField xPostionTF;
    @FXML
    private TextField yPositionTF;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> sirenOption = FXCollections.observableArrayList("YES", "NO");
        chBoxSiren.setItems(sirenOption);
        chBoxDoors.setCenterShape(true);
        ObservableList<String> doorsOption = FXCollections.observableArrayList("2", "3", "4", "5");
        chBoxDoors.setItems(doorsOption);
        capacityLL.setVisible(isCapacity);
        sirenLL.setVisible(isSiren);
        dorsLL.setVisible(isDoors);
        chBoxSiren.setVisible(isSiren);
        chBoxDoors.setVisible(isDoors);
        capacityTF.setVisible(isCapacity);
        if (type == 2) {
            policeFL.setVisible(true);
            policeFTF.setVisible(true);
            openPoliceBT.setVisible(true);
        }
        if (isChange) {
            changeVehicle();
            isChange = false;
        }
    }
    @FXML
    private boolean isSirenActivate() {
        return chBoxSiren.getSelectionModel().getSelectedItem().toString().equals("YES");
    }
    @FXML
    private void changeVehicle() {
        nameTF.setText(nameCG);
        chassisNameTF.setText(chassisNameCG);
        engineNumberTF.setText(engineNameCG);
        platformNumberTF.setText(Integer.toString(platformNumberCG));
        xPostionTF.setText(Integer.toString(xPositionCG));
        yPositionTF.setText(Integer.toString(yPositionCG));
        licencePlateTF.setText(licencePlateCG);
        Image imgage = new Image(img.toURI().toString());
        imageV.setImage(imgage);
        if (sirenCG) chBoxSiren.getSelectionModel().selectFirst();
        else chBoxSiren.getSelectionModel().selectLast();
        if (isDoors) chBoxDoors.getSelectionModel().select(Integer.toString(doorsNumberCG));
        if (isCapacity) capacityTF.setText(Integer.toString(capacityCG));
        if (type == 2) {
            policeFTF.setText(policeF);
        }
    }
    @FXML
    private void addVehicle() {
        boolean ready = false;
        try {
            if (!(nameTF.getText().isEmpty() || chassisNameTF.getText().isEmpty() || engineNumberTF.getText().isEmpty()
                    || (isSiren && chBoxSiren.getSelectionModel().getSelectedItem() == null) ||
                    (type == 2 && policeFTF.getText().isEmpty()) || imageV.getImage() == null) && checkXYcoordinate() && checkLicencePlate()
                    && checkPlatformNumber()) {
                String name = nameTF.getText();
                String chassisNumber = chassisNameTF.getText();
                String engineNumber = engineNumberTF.getText();
                String licencePlate = licencePlateTF.getText();
                int platformNumber = Integer.parseInt(platformNumberTF.getText());
                int xPosition = Integer.parseInt(xPostionTF.getText());
                int yPosition = Integer.parseInt(yPositionTF.getText());
                chackPlace(xPosition, yPosition, platformNumber);
                if (isSiren)
                    isSiren = isSirenActivate();
                File image = new File(imageV.getImage().getUrl().substring(6));
                if (isCapacity && checkCapacity()) {
                    if (!capacityTF.getText().isEmpty()) {
                        int capacity = Integer.parseInt(capacityTF.getText());
                        ControllerAdminMain.veh = new Van(isSiren, name, chassisNumber, engineNumber, licencePlate, image, type, platformNumber, xPosition, yPosition, capacity);
                        ready = true;
                    }
                } else if (isDoors) {
                    if (chBoxDoors.getSelectionModel().getSelectedItem() != null) {
                        int doorsNumber = Integer.parseInt(chBoxDoors.getSelectionModel().getSelectedItem().toString());
                        ControllerAdminMain.veh = new Car(isSiren, name, chassisNumber, engineNumber, licencePlate, image, type, platformNumber, xPosition, yPosition, doorsNumber);
                        ready = true;
                    }
                } else {
                    ControllerAdminMain.veh = new Motorcycle(isSiren, name, chassisNumber, engineNumber, licencePlate, image, type, platformNumber, xPosition, yPosition);
                    ready = true;
                }
                if (type == 2) ControllerAdminMain.veh.setPoliceFile(new File(policeFTF.getText()));
                if (ready)
                    addBT.getScene().getWindow().hide();
            } else {
                ControllerAdminMain.alertFunc("Error", "Enter all data!", Alert.AlertType.ERROR, ButtonType.OK);
            }
        } catch (BadInputException e) {
            ControllerAdminMain.alertFunc("Error!", e.getMessage(), Alert.AlertType.ERROR, ButtonType.OK);
        }
    }

    private boolean checkCapacity() throws BadInputException {
        String capacityTmp = capacityTF.getText();
        try {
            int number = Integer.parseInt(capacityTmp);
            if (number > 2000 && number < 4200) return true;
        } catch (NumberFormatException e) {
            throw new BadInputException("Please, input a number for capacity");
        }
        throw new BadInputException("Please, input value for capacity between 2000 and 4200");
    }
    private boolean checkXYcoordinate() throws BadInputException {
        String x = xPostionTF.getText();
        String y = yPositionTF.getText();
        int yCoo, xCoo;
        try {
            yCoo = Integer.parseInt(y);
        } catch (NumberFormatException e) {
            throw new BadInputException("Please, input a number for y coordinate!");
        }
        try {
            xCoo = Integer.parseInt(x);
        } catch (NumberFormatException e) {
            throw new BadInputException("Please, input a number for x coordinate!");
        }
        if ((yCoo == 0 || yCoo == 7) && xCoo > 1 && xCoo < 10) return true;
        else if ((yCoo == 3 || yCoo == 4) && xCoo > 1 && xCoo < 8) return true;
        throw new BadInputException("Please,input for y coordinate value \" 0 or 7 \" and for x between 1 and 8, for y coordinate \" 3 or 4 \" x between 1 and 10!");
    }

    private boolean checkLicencePlate() throws BadInputException {
        String licencePlateTmp = licencePlateTF.getText();
        if (licencePlateTmp.length() != 9) throw new BadInputException("Please, input valid licence plate!");
        for (int i = 0; i < 9; i++) {
            if (!(Character.isLetter(licencePlateTmp.charAt(i)) || licencePlateTmp.charAt(i) == '-' || Character.isDigit(licencePlateTmp.charAt(i))))
                throw new BadInputException("Please, input valid licence plate!");
            if (!(i == 0 || i == 4) && Character.isLetter(licencePlateTmp.charAt(i)))
                throw new BadInputException("Please, input valid licence plate!");
            else if (licencePlateTmp.charAt(i) == '-' && !(i == 3 || i == 5))
                throw new BadInputException("Please, input valid licence plate!");
            else if (Character.isDigit(licencePlateTmp.charAt(i)) && (i == 0 || i == 4 || i == 3 || i == 5))
                throw new BadInputException("Please, input valid licence plate!");
        }

        return true;
    }

    private boolean checkPlatformNumber() throws BadInputException {
        Properties property = new Properties();
        try {
            property.load(new FileReader("platform.properties"));
        } catch (IOException e) {
            throw new BadInputException("Properties file does not exist!");
        }
        int numberOfPlatforms = Integer.parseInt(property.getProperty("PlatformNumber"));
        int numberOfPlatformsForm;
        if (platformNumberTF.getText().length() == 0)
            throw new BadInputException("Please, input valid platform number between 0 and " + (numberOfPlatforms + 1));
        try {
            numberOfPlatformsForm = Integer.parseInt(platformNumberTF.getText());
        } catch (NumberFormatException e) {
            throw new BadInputException("Please, input valid platform number between 0 and " + (numberOfPlatforms + 1));
        }
        if (numberOfPlatformsForm < 1 || numberOfPlatformsForm > numberOfPlatforms)
            throw new BadInputException("Please, input valid platform number between 0 and " + (numberOfPlatforms + 1));
        return true;
    }

    private boolean chackPlace(int x, int y, int platform) throws BadInputException {
        for (Vehicle veh : ControllerAdminMain.listVehicle) {
            if (veh.getXPosition() == x && veh.getYPosition() == y && veh.getCurrentPlNumber() == platform)
                throw new BadInputException("Vehicle on this position already exist!");
        }
        return true;
    }
    @FXML
    private void openPicture() {
        FileChooser fileChsr = new FileChooser();
        fileChsr.setInitialDirectory(new File("Picture"));
        fileChsr.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JPG", "*.jpg"), new FileChooser.ExtensionFilter("PNG", "*.png"));
        File file = fileChsr.showOpenDialog(stage);
        if (file != null) {
            Image img = new Image(file.toURI().toString());
            imageV.setImage(img);
        }
    }
    @FXML
    private void openFile() {
        FileChooser fileChsr = new FileChooser();
        fileChsr.setInitialDirectory(new File(System.getProperty("user.home") + "/Police"));
        fileChsr.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TEXT", "*.txt"));
        File file = fileChsr.showOpenDialog(stage);
        if (file != null) {
            policeFTF.setText(file.toURI().toString().substring(6));
        }
    }
}
