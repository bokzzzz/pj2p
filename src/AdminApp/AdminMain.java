package AdminApp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.*;

public class AdminMain extends Application {
    public static Logger logger;
    static Stage stage;

    static {
        File homeDir = new File(System.getProperty("user.home"));
        if (!homeDir.exists()) homeDir.mkdir();
        File policeDir = new File(System.getProperty("user.home") + "/Police");
        if (!policeDir.exists()) policeDir.mkdir();
        File crashDir = new File(System.getProperty("user.home") + "/Police/CrashRecords");
        if (!crashDir.exists()) crashDir.mkdir();
        File recordsDir = new File(System.getProperty("user.home") + "/Police/Records");
        if (!recordsDir.exists()) recordsDir.mkdir();
        File policeFile = new File(System.getProperty("user.home") + "/Police/Policefile.txt");
        try {
            if (!policeFile.exists()) Files.createFile(policeFile.toPath());
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        File csvFile = new File("payments.csv");
        try {
            if (!csvFile.exists()) Files.createFile(csvFile.toPath());
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

    }
    public static void main(String[] args) {
        setLogger();
        launch(args);
    }

    private static void setLogger() {
        logger = Logger.getLogger("error.log");
        try {
            Handler fileHandler = new FileHandler("error.log", true);
            logger.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("mainFX.fxml"));
        primaryStage.setTitle("Garage");
        primaryStage.setScene(new Scene(root, 900, 550));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}
