module PJ2P {
    requires java.logging;
    requires annotations.java8;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    opens AdminApp;
    opens AdminService;
    opens AdminService.Vehicle;
    opens UserApp;

}