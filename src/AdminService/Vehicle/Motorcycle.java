package AdminService.Vehicle;

import java.io.File;

public class Motorcycle extends Vehicle {

    public Motorcycle(boolean isRotationOn, String name, String chassisNumber, String engineNumber, String licencePlate, File image, int type, int currentPlNumber, int XPosition, int YPosition) {
        super(isRotationOn, name, chassisNumber, engineNumber, licencePlate, image, type, currentPlNumber, XPosition, YPosition);
    }
}
