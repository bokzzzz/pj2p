package AdminService.Vehicle;

import AdminApp.AdminMain;
import AdminService.GarageOptions;
import AdminService.Platforms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;

public class TrafficCrashVehicle extends Thread {
    static final Random rnd = new Random();
    private static int sleepTime;
    private final VehicleSimulator vehicle;
    private final int platformCrash;
    private final int xPositionCrash;
    private final int yPositionCrash;
    private GarageOptions garageOptions;
    private boolean endFlag = true;
    private final VehicleSimulator[] crashCars = new VehicleSimulator[2];
    public TrafficCrashVehicle(int xPositionCrash, int yPositionCrash, int platformCrash, int type, VehicleSimulator crashCar1, VehicleSimulator crashCar2) {
        this.xPositionCrash = xPositionCrash;
        this.yPositionCrash = yPositionCrash;
        this.platformCrash = platformCrash;
        vehicle = new VehicleSimulator(VehicleGenerator.getVehicleCrash(type), GarageOptions.IN);
        garageOptions = GarageOptions.IN;
        crashCars[0] = crashCar1;
        crashCars[1] = crashCar2;
    }
    @Override
    public void run() {
        while (vehicle.getVehicle().getXPosition() == 0 && vehicle.getVehicle().getYPosition() == 0) {
            synchronized (VehicleSimulator.sync) {
                if (Platforms.parking[vehicle.getVehicle().getCurrentPlNumber() - 1].parkingVehicle[1][0] == null) {
                    vehicle.getVehicle().setXPosition(1);
                    Platforms.parking[vehicle.getVehicle().getCurrentPlNumber() - 1].parkingVehicle[1][0] = vehicle;
                }
            }
        }
        while (endFlag) {
            if (garageOptions == GarageOptions.IN) in();
            else out();
        }
    }
    private void out() {
        synchronized (VehicleSimulator.sync) {
            vehicle.setGarageOption(GarageOptions.OUT);
            Platforms.mapVehicleOut.add(vehicle);
            vehicle.start();
            endFlag = false;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }
    private void in() {
        boolean flagThread = false;
        synchronized (VehicleSimulator.sync) {
            int tmpX = vehicle.getVehicle().getXPosition(), tmpY = vehicle.getVehicle().getYPosition(), platform = vehicle.getVehicle().getCurrentPlNumber();
            if (Platforms.parking[platform - 1].parkingVehicle[tmpX][tmpY] == vehicle)
                Platforms.parking[platform - 1].parkingVehicle[tmpX][tmpY] = null;
            if (tmpY == 7 && vehicle.getVehicle().getCurrentPlNumber() < platformCrash) {
                platform++;
                tmpY = 0;
                tmpX = 1;
            } else if ((vehicle.getVehicle().getCurrentPlNumber()) < platformCrash || tmpY < yPositionCrash) tmpY++;
            else if (tmpX < xPositionCrash) tmpX++;
            else if (tmpX > xPositionCrash) tmpX--;
            vehicle.getVehicle().setXPosition(tmpX);
            vehicle.getVehicle().setYPosition(tmpY);
            vehicle.getVehicle().setCurrentPlNumber(platform);
            if (Platforms.parking[platform - 1].parkingVehicle[tmpX][tmpY] == null)
                Platforms.parking[platform - 1].parkingVehicle[tmpX][tmpY] = vehicle;
            if (tmpX == xPositionCrash && tmpY == yPositionCrash && platform == platformCrash) {
                if (vehicle.getVehicle().getType() == 2) {
                    writeCrash();
                    removeCrashCars();
                }
                Platforms.parking[platformCrash - 1].parkingVehicle[xPositionCrash][yPositionCrash] = vehicle;
                flagThread = true;
            }
        }
        if (flagThread) {
            try {
                if (sleepTime == 0) sleepTime = 1000 * (VehicleGenerator.rnd.nextInt(8) + 3);
                Thread.sleep(sleepTime);
                sleepTime = 0;
            } catch (InterruptedException e) {
                AdminMain.logger.log(Level.SEVERE, e.getMessage());
            }
            garageOptions = GarageOptions.OUT;
            Platforms.parking[vehicle.getVehicle().getCurrentPlNumber() - 1].running = true;
            Platforms.crashInPlatfom = false;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }
    private void writeCrash() {
        try {
            File f = new File(System.getProperty("user.home") + "/Police/CrashRecords/" + crashCars[0].getVehicle().getLicencePlate() + "_" + crashCars[1].getVehicle().getLicencePlate() + ".dat");
            FileOutputStream fOut = new FileOutputStream(f);
            ObjectOutputStream obj = new ObjectOutputStream(fOut);
            obj.write(("Name vehicle1:" + crashCars[0].getName()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(("Name vehicle2:" + crashCars[1].getName()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(("Licence plate vehicle1:" + crashCars[0].getVehicle().getLicencePlate()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(("Licence plate vehicle2:" + crashCars[1].getVehicle().getLicencePlate()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(("Police licence plate:" + vehicle.getVehicle().getLicencePlate()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(("Time:" + new Date().toString()).getBytes());
            obj.writeObject("relative path picture vehicle1: /Picture/" + crashCars[0].getVehicle().getImage().getName());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.writeObject("relative path picture vehicle2: /Picture/" + crashCars[1].getVehicle().getImage().getName());
            obj.close();
        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }
    private void removeCrashCars() {
        crashCars[0].setRunFlag(false);
        crashCars[1].setRunFlag(false);
        Platforms.parking[crashCars[0].getVehicle().getCurrentPlNumber() - 1].parkingVehicle[crashCars[0].getVehicle().getXPosition()][crashCars[0].getVehicle().getYPosition()] = null;
        Platforms.parking[crashCars[1].getVehicle().getCurrentPlNumber() - 1].parkingVehicle[crashCars[1].getVehicle().getXPosition()][crashCars[1].getVehicle().getYPosition()] = null;
    }
}
