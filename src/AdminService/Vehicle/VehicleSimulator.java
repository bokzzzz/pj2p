package AdminService.Vehicle;

import AdminApp.AdminMain;
import AdminService.GarageOptions;
import AdminService.Platforms;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class VehicleSimulator extends Thread {
    static final Object sync = new Object();
    private final Vehicle vehicle;
    private GarageOptions garageOptions;
    private boolean runFlag = true;
    private boolean crashFlag = true;
    public VehicleSimulator(Vehicle vehicle, GarageOptions garageOptions) {
        this.vehicle = vehicle;
        this.garageOptions = garageOptions;
    }

    void setRunFlag(boolean runFlag) {
        this.runFlag = runFlag;
    }
    @Override
    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
        while (runFlag) {
            if (vehicle.getType() == 2) {
                policeOut(vehicle.getXPosition(), vehicle.getYPosition());
            }
            synchronized (sync) {
                if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].running) {
                    if (vehicle.getType() == 2) {
                        policeOut(vehicle.getXPosition(), vehicle.getYPosition());
                    }
                    if (garageOptions != GarageOptions.IN && garageOptions != GarageOptions.OVERTAKINGIN) {
                        synchronized (sync) {
                            exitGarage();
                        }
                    } else if (garageOptions != GarageOptions.OUT && garageOptions != GarageOptions.OVERTAKINGOUT) {
                        inGarage();
                    }
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                AdminMain.logger.log(Level.SEVERE, e.getMessage());

            }
        }
    }
    public Vehicle getVehicle() {
        return vehicle;
    }

    private GarageOptions getGarageOption() {
        return garageOptions;
    }

    void setGarageOption(GarageOptions grOpt) {
        garageOptions = grOpt;
    }

    private int getPriorityVehicle() {
        if (vehicle.getType() == 0 && vehicle.isRotationOn()) return 0;
        else if (vehicle.getType() == 1 && vehicle.isRotationOn()) return 1;
        else if (vehicle.getType() == 2 && vehicle.isRotationOn()) return 2;
        return 3;

    }
    //Exit garage
    private void exitGarage() {
        int x = vehicle.getXPosition();
        int y = vehicle.getYPosition();
        if (garageOptions == GarageOptions.OUT) {
            if ((y == 0 || y == 4) && x > 1)
                y04xgreat(x, y);
            else if ((y == 1 || y == 5) && x > 1)
                y15xgreatExit(x, y);
            else if ((y == 3 || y == 7) && x > 1)
                y37xgreatExit(x, y);
            else if (x == 0 && y == 0)
                x0y0();
            else if (x == 0) x0ynot(y);
            else if (x == 1 && (y == 2 || y == 6)) x1y26(y);
            else if (x > 1 && (y == 2 || y == 6)) y26xgreatExit(x, y);
        } else if (garageOptions == GarageOptions.OVERTAKINGOUT)
            exitGarageOvertaking();
        if (vehicle.getCurrentPlNumber() == 0) {
            runFlag = false;
            if (vehicle.getType() == 3) {
                long endTimeInGarage = System.currentTimeMillis();
                Platforms.chargeParking(endTimeInGarage - vehicle.getStartTimeInGarage(), this);
            }
        }
    }
    private void exitGarageOvertaking() {
        int x = vehicle.getXPosition();
        int y = vehicle.getYPosition();
        if (x == 1) x1Overtaking(y);
        if (x > 1 && (y == 1 || y == 5)) y15OverTaking(x, y);
    }
    private void y04xgreat(int x, int y) {
        if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] == null
                && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 2] == null || Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 2].getGarageOption() != GarageOptions.STOP)
                && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y + 1] == null || (getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y + 1].getPriorityVehicle() &&
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y + 1].getGarageOption() != GarageOptions.STOP))
                ) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
            y++;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
            vehicle.setYPosition(y);
            vehicle.setXPosition(x);
        } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] != null && getPriorityVehicle() == 3 &&
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1].getPriorityVehicle() == 3 && TrafficCrashVehicle.rnd.nextInt(100) >= 90 && crashFlag && !Platforms.crashInPlatfom) {
            Platforms.callPolFireAmb(this, Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1]);
            crashFlag = false;
            Platforms.crashInPlatfom = true;
        }

    }
    private void y15xgreatExit(int x, int y) {
        if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] == null &&
                (x == 9 || (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y + 1] == null || (getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y + 1].getPriorityVehicle()
                        && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y + 1].getGarageOption() != GarageOptions.STOP))) &&
                (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 2] == null || (getPriorityVehicle() <= Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 2].getPriorityVehicle() || Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 2].getGarageOption() == GarageOptions.PARK))) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
            y++;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
            vehicle.setYPosition(y);
            vehicle.setXPosition(x);
        } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] != null && getPriorityVehicle() == 3 &&
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1].getPriorityVehicle() == 3 && TrafficCrashVehicle.rnd.nextInt(100) >= 90 && crashFlag && !Platforms.crashInPlatfom) {
            Platforms.callPolFireAmb(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1], this);
            crashFlag = false;
            Platforms.crashInPlatfom = true;
        }
    }
    private void y37xgreatExit(int x, int y) {
        if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1] == null &&
                (x == 9 || Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y - 1] == null || (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y - 1].getGarageOption() != GarageOptions.STOP &&
                        getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y - 1].getPriorityVehicle())) &&
                (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 2] == null || (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 2].getGarageOption() != GarageOptions.STOP &&
                        getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 2].getPriorityVehicle()))) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
            y--;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
            vehicle.setYPosition(y);
            vehicle.setXPosition(x);
        } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1] != null && getPriorityVehicle() == 3 &&
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1].getPriorityVehicle() == 3 && VehicleGenerator.rnd.nextInt(100) >= 90 && TrafficCrashVehicle.rnd.nextInt(100) >= 90 && crashFlag && !Platforms.crashInPlatfom) {
            Platforms.callPolFireAmb(this, Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1]);
            crashFlag = false;
            Platforms.crashInPlatfom = true;
        }
    }
    private void x0ynot(int y) {
        if (y < 7 && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y + 1] != null && getPriorityVehicle() > Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y + 1].getPriorityVehicle())
                && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y - 1] == null && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y - 1] == null && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] == null) {
            garageOptions = GarageOptions.STOP;
        } else {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y - 1] == null) {
                if (y == 3 || y == 7) {
                    if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y - 1] == null ||
                            getPriorityVehicle() <= Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y - 1].getPriorityVehicle()
                            || Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y - 1].getGarageOption() == GarageOptions.IN) {
                        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = null;
                        y--;
                        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = this;
                    }
                } else {
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = null;
                    y--;
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = this;
                }
            } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y - 1].getGarageOption() == GarageOptions.STOP) {
                garageOptions = GarageOptions.OVERTAKINGOUT;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = null;
                y--;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = this;
                vehicle.setXPosition(1);
            } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y - 1] != null && getPriorityVehicle() == 3 &&
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y - 1].getPriorityVehicle() == 3 && TrafficCrashVehicle.rnd.nextInt(100) >= 90 && crashFlag && !Platforms.crashInPlatfom) {
                Platforms.callPolFireAmb(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y - 1], this);
                crashFlag = false;
                Platforms.crashInPlatfom = true;
            }
        }
        vehicle.setYPosition(y);
    }
    private void x1y26(int y) {
        if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] == null && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y + 1] == null
                || getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y + 1].getPriorityVehicle())) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = null;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = this;
            vehicle.setXPosition(0);
        } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] != null && getPriorityVehicle() == 3 &&
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y].getPriorityVehicle() == 3 && TrafficCrashVehicle.rnd.nextInt(100) >= 90 && crashFlag && !Platforms.crashInPlatfom) {
            Platforms.callPolFireAmb(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y], this);
            crashFlag = false;
            Platforms.crashInPlatfom = true;
        }
    }
    private void y26xgreatExit(int x, int y) {
        if (x > 2 && (x < 9 && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y] != null && getPriorityVehicle() > Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y].getPriorityVehicle())
                && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1] == null && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y] == null &&
                (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 2][y - 1] == null || Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 2][y - 1].getGarageOption() != GarageOptions.STOP)) {
            garageOptions = GarageOptions.STOP;
        } else {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y] == null) {
                if ((Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y - 1] == null || getPriorityVehicle() <= Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y - 1].getPriorityVehicle()) &&
                        (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y + 1] == null || Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y + 1].getGarageOption() == GarageOptions.PARK || getPriorityVehicle() <= Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y + 1].getPriorityVehicle())) {
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
                    x--;
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
                }

            } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y].getGarageOption() == GarageOptions.STOP &&
                    getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y].getPriorityVehicle()) {
                garageOptions = GarageOptions.OVERTAKINGOUT;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
                x--;
                y--;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
            } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y] != null && getPriorityVehicle() == 3 &&
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y].getPriorityVehicle() == 3 && TrafficCrashVehicle.rnd.nextInt(100) >= 90 && crashFlag && !Platforms.crashInPlatfom) {
                Platforms.callPolFireAmb(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y], this);
                crashFlag = false;
                Platforms.crashInPlatfom = true;
            }
        }
        vehicle.setXPosition(x);
        vehicle.setYPosition(y);
    }
    private void x1Overtaking(int y) {
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y].setGarageOption(GarageOptions.OUT);
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = null;
        garageOptions = GarageOptions.OUT;
        y--;
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = this;
        vehicle.setYPosition(y);
        vehicle.setXPosition(0);
    }
    private void x0y0() {
        if (vehicle.getCurrentPlNumber() > 1) {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 2].parkingVehicle[0][7] == null) {
                vehicle.setYPosition(7);
                Platforms.parking[vehicle.getCurrentPlNumber() - 2].parkingVehicle[0][7] = this;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][0] = null;
                vehicle.setCurrentPlNumber(vehicle.getCurrentPlNumber() - 1);
            }
        } else {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][0] = null;
            vehicle.setCurrentPlNumber(vehicle.getCurrentPlNumber() - 1);
        }

    }
    private void y15OverTaking(int x, int y) {
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1].setGarageOption(GarageOptions.OUT);
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
        garageOptions = GarageOptions.OUT;
        x--;
        y++;
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
        vehicle.setYPosition(y);
        vehicle.setXPosition(x);
    }
    //Parking vehicle
    private void inGarage() {
        if (garageOptions == GarageOptions.IN) {
            int x = vehicle.getXPosition();
            int y = vehicle.getYPosition();
            if (x == 0 && y == 0 && vehicle.getCurrentPlNumber() == -1) {
                setFistCoordinate();
            } else if (x == 1 && (y == 1 || y == 5)) x1y15(y);
            else if ((y == 1 || y == 5) && x > 1) xgreaty15(x, y);
            else if ((y == 2 || y == 6) && x > 1) xgreaty26(x, y);
            else if (x == 1 && y == 7) x1y7();
            else if (x == 1 && y < 7) x1ygreat(y);
        }
        if (isParked(vehicle.getXPosition(), vehicle.getYPosition())) {
            Platforms.mapVehiclePark.add(vehicle);
            runFlag = false;
        }
    }
    private void x1ygreat(int y) {
        if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y + 1] == null) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = null;
            y++;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = this;
        } else {
            if (y > 0 && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y - 1] != null && getPriorityVehicle() > Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y - 1].getPriorityVehicle())
                    && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y + 1] == null && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y + 1] == null && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] == null) {
                garageOptions = GarageOptions.STOP;
            } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y + 1].getGarageOption() == GarageOptions.STOP) {
                garageOptions = GarageOptions.OVERTAKINGOUT;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = null;
                y++;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[0][y] = this;
                vehicle.setXPosition(0);
            }
        }
        vehicle.setYPosition(y);
    }
    private void x1y15(int y) {
        if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[2][y] == null && ((isFree03() && y == 1) || (y == 5 && isFree47()))) {
            {
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = null;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[2][y] = this;
                vehicle.setXPosition(2);

            }
        } else {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y + 1] == null && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[2][y + 1] == null ||
                    getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[2][y + 1].getPriorityVehicle())) {
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = null;
                y++;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][y] = this;
            }
        }
        vehicle.setYPosition(y);
    }
    private void xgreaty15(int x, int y) {
        if (((y == 5 && x < 8) || (y == 1)) && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1] == null) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
            y--;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
            vehicle.setYPosition(y);
            garageOptions = GarageOptions.PARK;
            Platforms.mapVehicleIn.remove(this);
        } else if (((y == 1 && x < 8) || (y == 5)) && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 2] == null && (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] == null ||
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1].getGarageOption() == GarageOptions.OUT)) {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] == null && (x > 8 || (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y + 1] == null || getPriorityVehicle() < Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y + 1].getPriorityVehicle()))) {
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
                y++;
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
                vehicle.setYPosition(y);
            }
        } else if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y] == null) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
            x++;
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
            vehicle.setXPosition(x);
        }

    }
    private void x1y7() {
        if (Platforms.parking[vehicle.getCurrentPlNumber()].parkingVehicle[1][0] == null) {
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][7] = null;
            vehicle.setCurrentPlNumber(vehicle.getCurrentPlNumber() + 1);
            vehicle.setYPosition(0);
            vehicle.setXPosition(1);
            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[1][0] = this;
        }
    }
    private void xgreaty26(int x, int y) {
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = null;
        y++;
        vehicle.setYPosition(y);
        Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] = this;
        garageOptions = GarageOptions.PARK;
        Platforms.mapVehicleIn.remove(this);
    }
    private boolean isFree03() {
        int freeSpace = 0, vehicleIn = 0;
        for (int i = 2; i < 10; i++) {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][0] == null) freeSpace++;
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][3] == null && i < 8) freeSpace++;
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][1] != null &&
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][1].getGarageOption() != GarageOptions.OVERTAKINGOUT)
                vehicleIn++;
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][2] != null &&
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][2].getGarageOption() == GarageOptions.IN)
                vehicleIn++;
        }
        return vehicleIn < freeSpace;
    }
    private boolean isFree47() {
        int freeSpace = 0, vehicleIn = 0;
        for (int i = 2; i < 10; i++) {
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][7] == null) freeSpace++;
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][4] == null && i < 8) freeSpace++;
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][5] != null &&
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][5].getGarageOption() != GarageOptions.OVERTAKINGOUT)
                vehicleIn++;
            if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][6] != null &&
                    Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[i][6].getGarageOption() == GarageOptions.IN)
                vehicleIn++;
        }
        return vehicleIn < freeSpace;
    }
    public void setFistCoordinate() {
        if (isOpenGarage()) {
            Platforms.parking[0].parkingVehicle[1][0] = this;
            vehicle.setCurrentPlNumber(1);
            vehicle.setXPosition(1);
            Platforms.mapVehicleIn.add(this);
            start();
        }
    }
    private boolean isParked(int x, int y) {
        return (x > 1 && (y == 0 || y == 3 || y == 4 || y == 7));
    }
    private boolean isOpenGarage() {
        int numberOfVehicleIn = Platforms.mapVehicleIn.size(), numberOfParkVehicle = 0;
        for (int i = 0; i < Platforms.parking.length; i++) {
            for (int j = 2; j < 10; j++) {
                if (Platforms.parking[i].parkingVehicle[j][0] == null) numberOfParkVehicle++;
                if (Platforms.parking[i].parkingVehicle[j][7] == null) numberOfParkVehicle++;
                if (j < 8 && Platforms.parking[i].parkingVehicle[j][3] == null) numberOfParkVehicle++;
                if (j < 8 && Platforms.parking[i].parkingVehicle[j][4] == null) numberOfParkVehicle++;
            }
        }
        return numberOfVehicleIn < numberOfParkVehicle;
    }
    //police
    private void policeOut(int x, int y) {
        if (x > 0 && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x - 1][y] != null) {
            followPolice(x - 1, y);
        }
        if (x < 9 && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x + 1][y] != null) {
            followPolice(x + 1, y);
        }
        if (y > 0 && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y - 1] != null) {
            followPolice(x, y - 1);
        }
        if (y < 7 && Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y + 1] != null) {
            followPolice(x, y + 1);
        }
    }
    private void followPolice(int x, int y) {
        boolean openFile = false;
        try {
            List<String> licencePlates = Files.readAllLines(Paths.get(vehicle.getPoliceFile().getAbsolutePath()));
            synchronized (sync) {
                if (Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y] != null) {
                    for (String licencePlate : licencePlates) {
                        if (licencePlate.equals(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y].getVehicle().getLicencePlate())) {
                            if (!Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y].isAlive()) {
                                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y].start();
                                Platforms.mapVehicleOut.add(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y]);
                            }
                            Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y].setGarageOption(GarageOptions.STOP);
                            licencePlates.remove(licencePlate);
                            openFile = true;
                            recordVehicle(Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y].vehicle, vehicle);
                            break;
                        }
                    }
                }
            }
            if (openFile) {
                try (BufferedWriter bf = new BufferedWriter(new FileWriter(new File(vehicle.getPoliceFile().getAbsolutePath())))) {
                    for (String licencePlate : licencePlates) {
                        bf.write(licencePlate);
                        bf.write(System.getProperty("line.separator"));
                    }
                }
                try {
                    Random rnd = new Random();
                    Thread.sleep((rnd.nextInt(3) + 3) * 1000);
                } catch (InterruptedException e) {
                    AdminMain.logger.log(Level.SEVERE, e.getMessage());
                }
                Platforms.parking[vehicle.getCurrentPlNumber() - 1].parkingVehicle[x][y].setGarageOption(GarageOptions.OUT);
            }

        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }
    private void recordVehicle(Vehicle veh, Vehicle police) {
        try {
            File f = new File(System.getProperty("user.home") + "/Police/Records/" + veh.getLicencePlate() + ".dat");
            FileOutputStream fOut = new FileOutputStream(f);
            ObjectOutputStream obj = new ObjectOutputStream(fOut);
            obj.write(("Name vehicle:" + veh.getName()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(("Police licence plate:" + police.getLicencePlate()).getBytes());
            obj.write((System.getProperty("line.separator")).getBytes());
            obj.write(new Date().toString().getBytes());
            obj.writeObject("relative path: /Picture/" + veh.getImage().getName());
            obj.close();
        } catch (Exception e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
    }
}
