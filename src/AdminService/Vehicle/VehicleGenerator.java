package AdminService.Vehicle;

import AdminService.GarageOptions;
import AdminService.Platforms;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Random;

public class VehicleGenerator {
    static final Random rnd = new Random();
    private static final int MAXCAPACITY = 4000;
    private static final int MINCAPACITY = 800;
    private static final int MAXNUMBEROFDOORS = 5;
    private static final int MINNUMBEROFDOORS = 2;
    private static final String symbols = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    private static final String digits = "0123456789";

    public static VehicleSimulator createRandomVehicle(GarageOptions garageOptions, int platform) {
        VehicleSimulator vehSim;
        int type = rnd.nextInt(10);
        int veh = rnd.nextInt(3);
        int typeVeh, x = 0, y = 0;
        boolean flag = false;
        if (garageOptions == GarageOptions.OUT || garageOptions == GarageOptions.PARK) {
            do {
                x = rnd.nextInt(8) + 2;
                y = rnd.nextInt(8);
                if (y == 1) y--;
                else if (y == 6) y++;
                else if ((y == 3 || y == 4) && x > 7) x -= (x - 7);
                else if (y == 2 && x > 7) {
                    x -= (x - 7);
                    y++;
                } else if (y == 5 && x > 7) {
                    x -= (x - 7);
                    y--;
                } else if (y == 2) y++;
                else if (y == 5) y--;
            } while (Platforms.parking[platform - 1].parkingVehicle[x][y] != null);
        }
        if (type > 0) {
            typeVeh = 3;
        } else {
            typeVeh = rnd.nextInt(3);
            if ((garageOptions == GarageOptions.OUT || garageOptions == GarageOptions.PARK) && rnd.nextInt(2) == 1)
                flag = true;
        }
        if (veh == 0) {
            if (typeVeh < 2) typeVeh = 2;
            Motorcycle motor = new Motorcycle(flag, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(veh, typeVeh)), typeVeh, platform, x, y);
            vehSim = new VehicleSimulator(motor, garageOptions);
        } else if (veh == 1) {
            if (typeVeh == 1)
                if (rnd.nextInt(2) == 1) typeVeh = 2;
                else typeVeh = 0;
            Car motor = new Car(flag, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(veh, typeVeh)), typeVeh, platform, x, y, rnd.nextInt(MAXNUMBEROFDOORS - MINNUMBEROFDOORS) + MINNUMBEROFDOORS);
            vehSim = new VehicleSimulator(motor, garageOptions);
        } else {
            Van motor = new Van(flag, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(veh, typeVeh)), typeVeh, platform, x, y, rnd.nextInt(MAXCAPACITY - MINCAPACITY) + MINCAPACITY);
            vehSim = new VehicleSimulator(motor, garageOptions);
        }
        if (vehSim.getVehicle().getType() == 2) {
            File file = new File(System.getProperty("user.home") + "/Police/Policefile.txt");
            vehSim.getVehicle().setPoliceFile(file);
        }
        return vehSim;
    }

    @NotNull
    private static String getRandomName() {
        int MAXNAMELENGTH = 15;
        int length = rnd.nextInt(MAXNAMELENGTH) + 1;
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < length; i++)
            name.append(symbols.charAt(rnd.nextInt(symbols.length())));
        return name.toString();
    }

    @NotNull
    private static String getRandomChassisNumber() {
        int length = 17;
        StringBuilder chassisNumber = new StringBuilder();
        for (int i = 0; i < length; i++) {
            if (rnd.nextInt(2) == 1) chassisNumber.append(symbols.charAt(rnd.nextInt(symbols.length())));
            else chassisNumber.append(digits.charAt(rnd.nextInt(digits.length())));
        }
        return chassisNumber.toString();
    }

    @NotNull
    private static String getRandomEngineNumber() {
        int length = 10;
        StringBuilder engineNumber = new StringBuilder();
        for (int i = 0; i < length; i++)
            if (rnd.nextInt(2) == 1) engineNumber.append(symbols.charAt(rnd.nextInt(symbols.length())));
            else engineNumber.append(digits.charAt(rnd.nextInt(digits.length())));
        return engineNumber.toString();
    }

    @NotNull
    private static String getRandomLicencePlate() {
        StringBuilder licencePlate = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            if (i == 0 || i == 4)
                licencePlate.append(symbols.charAt(rnd.nextInt(symbols.length())));
            else if (i == 3 || i == 5) licencePlate.append("-");
            else licencePlate.append(digits.charAt(rnd.nextInt(digits.length())));
        }
        return licencePlate.toString();
    }

    @NotNull
    private static String getFileName(int veh, int vehType) {
        return String.valueOf(veh) +
                vehType +
                rnd.nextInt(2) +
                ".jpg";
    }

    static Vehicle getVehicleCrash(int type) {
        Vehicle vehicle;
        if (type == 1) {
            vehicle = new Van(true, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(2, 1)), 1, 1, 0, 0, rnd.nextInt(MAXCAPACITY - MINCAPACITY) + MINCAPACITY);
        } else if (type == 0) {
            int typeVehicle = rnd.nextInt(2);
            if (typeVehicle == 0) {
                vehicle = new Car(true, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(2, 0)), 0, 1, 0, 0, rnd.nextInt(MAXNUMBEROFDOORS - MINNUMBEROFDOORS) + MINNUMBEROFDOORS);
            } else {
                vehicle = new Van(true, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(1, 0)), 0, 1, 0, 0, rnd.nextInt(MAXCAPACITY - MINCAPACITY) + MINCAPACITY);
            }
        } else {
            int typeVehicle = rnd.nextInt(3);
            if (typeVehicle == 0) {
                vehicle = new Car(true, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(2, 2)), 2, 1, 0, 0, rnd.nextInt(MAXNUMBEROFDOORS - MINNUMBEROFDOORS) + MINNUMBEROFDOORS);
            } else if (typeVehicle == 1) {
                vehicle = new Motorcycle(true, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(0, 2)), 2, 1, 0, 0);
            } else {
                vehicle = new Van(true, getRandomName(), getRandomChassisNumber(), getRandomEngineNumber(), getRandomLicencePlate(), new File(getFileName(1, 2)), 2, 1, 0, 0, rnd.nextInt(MAXCAPACITY - MINCAPACITY) + MINCAPACITY);
            }
        }
        if (vehicle.getType() == 2) {
            File file = new File(System.getProperty("user.home") + "/Police/Policefile.txt");
            vehicle.setPoliceFile(file);
        }
        return vehicle;
    }
}
