package AdminService.Vehicle;

import java.io.File;

public class Car extends Vehicle {


    private int numberOfDors;

    public Car(boolean isRotationOn, String name, String chassisNumber, String engineNumber, String licencePlate, File image, int type, int currentPlNumber, int XPosition, int YPosition, int numberOfDors) {
        super(isRotationOn, name, chassisNumber, engineNumber, licencePlate, image, type, currentPlNumber, XPosition, YPosition);
        this.numberOfDors = numberOfDors;
    }

    public int getNumberOfDors() {
        return numberOfDors;
    }

}
