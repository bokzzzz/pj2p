package AdminService.Vehicle;

import java.io.File;
import java.io.Serializable;


public class Vehicle implements Serializable {
    private boolean isRotationOn;
    private String name, chassisNumber, engineNumber, licencePlate;
    private File image;
    private int type;
    private int currentPlNumber;
    private int XPosition;
    private int YPosition;
    private File policeFile;

    private final long startTimeInGarage;

    Vehicle(boolean isRotationOn, String name, String chassisNumber, String engineNumber, String licencePlate, File image, int type, int currentPlNumber, int XPosition, int YPosition) {
        this.isRotationOn = isRotationOn;
        this.name = name;
        this.chassisNumber = chassisNumber;
        this.engineNumber = engineNumber;
        this.licencePlate = licencePlate;
        this.image = image;
        this.type = type;
        this.currentPlNumber = currentPlNumber;
        this.XPosition = XPosition;
        this.YPosition = YPosition;
        startTimeInGarage = System.currentTimeMillis();
    }

    long getStartTimeInGarage() {
        return startTimeInGarage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isRotationOn() {
        return isRotationOn;
    }

    public void setRotationOn(boolean rotationOn) {
        if (type < 4)
            isRotationOn = rotationOn;
    }

    public int getCurrentPlNumber() {
        return currentPlNumber;
    }

    public void setCurrentPlNumber(int currentPlNumber) {
        this.currentPlNumber = currentPlNumber;
    }

    public int getXPosition() {
        return XPosition;
    }

    void setXPosition(int XPosition) {
        this.XPosition = XPosition;
    }

    public int getYPosition() {
        return YPosition;
    }

    void setYPosition(int YPosition) {
        this.YPosition = YPosition;
    }

    public String getParkingName() {
        switch (type) {
            case 0:
                if (isRotationOn) return "HR";
                else return " H";
            case 2:
                if (isRotationOn) return "PR";
                else return " P";
            case 1:
                if (isRotationOn) return "FR";
                else return " F";
            case 3:
                return " V";
        }
        return "";
    }

    public File getPoliceFile() {
        return policeFile;
    }

    public void setPoliceFile(File policeFile) {
        if (type == 2)
            this.policeFile = policeFile;
    }
}
