package AdminService.Vehicle;

import java.io.File;

public class Van extends Vehicle {


    private int capacity;

    public Van(boolean isRotationOn, String name, String chassisNumber, String engineNumber, String licencePlate, File image, int type, int currentPlNumber, int XPosition, int YPosition, int capacity) {
        super(isRotationOn, name, chassisNumber, engineNumber, licencePlate, image, type, currentPlNumber, XPosition, YPosition);
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
