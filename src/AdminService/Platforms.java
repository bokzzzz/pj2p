package AdminService;

import AdminApp.AdminMain;
import AdminApp.ControllerAdminMain;
import AdminService.Vehicle.TrafficCrashVehicle;
import AdminService.Vehicle.Vehicle;
import AdminService.Vehicle.VehicleGenerator;
import AdminService.Vehicle.VehicleSimulator;
import UserApp.ControllerUserApp;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class Platforms {
    public static boolean crashInPlatfom = false;
    public static Parking[] parking;
    public static List<VehicleSimulator> mapVehicleOut;
    public static List<VehicleSimulator> mapVehicleIn;
    public static List<Vehicle> mapVehiclePark;
    private static PrintWriter printWriter;

    static {
        try {
            printWriter = new PrintWriter(new FileWriter("payments.csv", true), true);
        } catch (IOException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }

    }

    public Platforms(int platformNumbers, int numberVehiclesPlatform) {
        mapVehicleOut = new ArrayList<>();
        mapVehicleIn = new ArrayList<>();
        mapVehiclePark = new ArrayList<>();
        parking = new Parking[platformNumbers];
        for (int i = 0; i < platformNumbers; i++)
            parking[i] = new Parking();
        Random rnd = new Random();
        int[] numberOfAddVehilce = new int[platformNumbers];
        for (Vehicle veh : ControllerAdminMain.listVehicle) {

            if (numberOfAddVehilce[veh.getCurrentPlNumber() - 1] < numberVehiclesPlatform && veh.getCurrentPlNumber() < platformNumbers) {
                VehicleSimulator vehSim;
                if (rnd.nextInt(100) > 85) {
                    vehSim = new VehicleSimulator(veh, GarageOptions.OUT);
                    mapVehicleOut.add(vehSim);
                } else {
                    vehSim = new VehicleSimulator(veh, GarageOptions.PARK);
                    mapVehiclePark.add(veh);
                }
                parking[veh.getCurrentPlNumber() - 1].parkingVehicle[veh.getXPosition()][veh.getYPosition()] = vehSim;
                numberOfAddVehilce[veh.getCurrentPlNumber() - 1]++;
            }
        }
        for (int i = 0; i < platformNumbers; i++) {
            for (int j = numberOfAddVehilce[i]; j < numberVehiclesPlatform; j++) {
                VehicleSimulator veh;
                if (rnd.nextInt(100) > 85) {
                    veh = VehicleGenerator.createRandomVehicle(GarageOptions.OUT, i + 1);
                    mapVehicleOut.add(veh);
                } else {
                    veh = VehicleGenerator.createRandomVehicle(GarageOptions.PARK, i + 1);
                    mapVehiclePark.add(veh.getVehicle());
                }
                parking[veh.getVehicle().getCurrentPlNumber() - 1].parkingVehicle[veh.getVehicle().getXPosition()][veh.getVehicle().getYPosition()] = veh;
            }
        }
        mapVehicleOut.parallelStream().forEach(Thread::start);
    }

    public static void addVehicleIn() {
        if (Platforms.parking[0].parkingVehicle[1][0] == null) {
            VehicleSimulator vehSim = VehicleGenerator.createRandomVehicle(GarageOptions.IN, -1);
            vehSim.setFistCoordinate();
        }
    }

    public static void callPolFireAmb(VehicleSimulator veh1, VehicleSimulator veh2) {
        Platforms.parking[veh1.getVehicle().getCurrentPlNumber() - 1].running = false;
        TrafficCrashVehicle police = new TrafficCrashVehicle(veh2.getVehicle().getXPosition(), veh2.getVehicle().getYPosition(), veh2.getVehicle().getCurrentPlNumber(), 2, veh1, veh2);
        TrafficCrashVehicle ambulance = new TrafficCrashVehicle(veh2.getVehicle().getXPosition(), veh2.getVehicle().getYPosition(), veh2.getVehicle().getCurrentPlNumber(), 0, veh1, veh2);
        TrafficCrashVehicle firecar = new TrafficCrashVehicle(veh2.getVehicle().getXPosition(), veh2.getVehicle().getYPosition(), veh2.getVehicle().getCurrentPlNumber(), 1, veh1, veh2);
        police.start();
        ambulance.start();
        firecar.start();
    }

    public boolean isRunning() {
        return mapVehicleOut.parallelStream().anyMatch(Thread::isAlive) || mapVehicleIn.parallelStream().anyMatch(Thread::isAlive) || ControllerUserApp.running;
    }

    public static void chargeParking(long time, VehicleSimulator veh) {
        double hour = time / 1000 / 3600;
        if (hour <= 1.0) {
            printWriter.println(veh.getVehicle().getLicencePlate() + "," + "1.0 KM");
        } else if (hour <= 3.0) {
            printWriter.println(veh.getVehicle().getLicencePlate() + "," + "2.0 KM");
        } else {
            printWriter.println(veh.getVehicle().getLicencePlate() + "," + "8.0 KM");
        }

    }

    public static void serializeEnd() {
        try {
            FileOutputStream fos = new FileOutputStream("garage.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(mapVehiclePark);
            oos.close();
            fos.close();
        } catch (IOException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }

    }

    public String getMatPos(int curPlat) {
        return parking[curPlat - 1].getText();
    }
}
