package AdminService;

public enum GarageOptions {
    OUT, IN, STOP, OVERTAKINGOUT, OVERTAKINGIN, PARK
}
