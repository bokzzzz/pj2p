package AdminService;

import AdminService.Vehicle.VehicleSimulator;

public class Parking {
    public final VehicleSimulator[][] parkingVehicle;
    public boolean running = true;
    private final String str;
    private final String[][] parkingMat;

    Parking() {
        this.parkingMat = new String[10][8];
        str = "                \n" +
                "                \n" +
                "**    ****    **\n" +
                "**    ****    **\n" +
                "**    ****    **\n" +
                "**    ****    **\n" +
                "**    ****    **\n" +
                "**    ****    **\n" +
                "**            **\n" +
                "**            **\n";
        setMat();
        this.parkingVehicle = new VehicleSimulator[10][8];
    }

    String getText() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 8; j++) {
                if (parkingVehicle[i][j] != null) {
                    str.append(parkingVehicle[i][j].getVehicle().getParkingName());
                } else str.append(parkingMat[i][j]);
            }
            str.append('\n');
        }
        return str.toString();
    }

    private void setMat() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 9; j++) {
                if (str.charAt(i * 17 + j * 2) != '\n' && str.charAt(i * 17 + j * 2 + 1) != '\n') {
                    parkingMat[i][j] = Character.toString(str.charAt(i * 17 + j * 2)) + str.charAt(i * 17 + j * 2 + 1);
                }
            }
        }
    }
}
