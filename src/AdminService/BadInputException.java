package AdminService;

public class BadInputException extends Exception {
    public BadInputException(String message) {
        super(message);
    }
}
