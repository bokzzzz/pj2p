package UserApp;

import AdminApp.AdminMain;
import AdminService.Platforms;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class ControllerUserApp implements Initializable {
    private int platOld = 0;
    private int numberPlatforms;
    public static boolean running = true;
    @FXML
    private ComboBox<String> strCB;
    @FXML
    private ComboBox<Integer> vehicleCB;
    @FXML
    private TextArea[] text;
    @FXML
    private AnchorPane pane;
    @FXML
    private Button addVehicle;
    @FXML
    private Button startBut;

    @FXML
    private Button stopButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setPlatform();
        ObservableList<Integer> vehicleCBox = FXCollections.observableArrayList();
        for (int i = 0; i < 29; i++)
            vehicleCBox.add(i);
        vehicleCB.setItems(vehicleCBox);
    }

    @FXML
    private void setPlatform() {
        Properties property = new Properties();
        try {
            property.load(new FileReader("platform.properties"));
            numberPlatforms = Integer.parseInt(property.getProperty("PlatformNumber"));
        } catch (IOException e) {
            AdminMain.logger.log(Level.SEVERE, e.getMessage());
        }
        text = new TextArea[numberPlatforms];
        for (int i = 0; i < numberPlatforms; i++) {
                text[i] = new TextArea();
                text[i].setPrefWidth(293);
            text[i].setPrefHeight(297);
            text[i].setLayoutX(64);
            text[i].setLayoutY(229);
                text[i].setText(Integer.toString(i + 1));
                text[i].setVisible(false);
                pane.getChildren().add(text[i]);
                strCB.getItems().add(Integer.toString(i + 1));
                text[i].setPrefColumnCount(1);
                text[i].setWrapText(true);
                text[i].setPrefRowCount(10);
                text[i].setEditable(false);
                text[i].setText("                \n" +
                        "                \n" +
                        "**    ****    **\n" +
                        "**    ****    **\n" +
                        "**    ****    **\n" +
                        "**    ****    **\n" +
                        "**    ****    **\n" +
                        "**    ****    **\n" +
                        "**            **\n" +
                        "**            **\n");
            text[i].setFont(Font.font("Monospaced", 21));
            }
    }

    @FXML
    private void clickOnPlatform() {
        text[platOld].setVisible(false);
        int plat = Integer.parseInt(strCB.getSelectionModel().getSelectedItem());
        platOld = plat - 1;
        text[platOld].setVisible(true);

    }

    @FXML
    private void start() {
        startBut.setDisable(true);
        Platforms platforms = new Platforms(numberPlatforms, vehicleCB.getSelectionModel().getSelectedItem());
        for (int i = 0; i < numberPlatforms; i++) text[i].setText(platforms.getMatPos(i + 1));
        addVehicle.setDisable(false);
        strCB.setDisable(false);
        stopButton.setDisable(false);
        Thread thr = new Thread(() -> {
            while (platforms.isRunning()) {
                Platform.runLater(() -> text[platOld].setText(platforms.getMatPos(platOld + 1)));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    AdminMain.logger.log(Level.SEVERE, e.getMessage());
                }
            }
            text[platOld].setText(platforms.getMatPos(platOld + 1));
            strCB.setDisable(true);
            Platforms.serializeEnd();
        });
        thr.setPriority(8);
        thr.start();
    }

    @FXML
    private void addVehicle() {
        Platforms.addVehicleIn();
    }

    @FXML
    private void activateButton() {
        startBut.setDisable(false);
    }

    @FXML
    private void actionOnStop() {
        stopButton.setDisable(true);
        addVehicle.setDisable(true);
        running = false;
    }
}
